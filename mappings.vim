

set noswapfile
" forever pending operator-mode
" easier on laptop
set notimeout
set noswapfile

set expandtab
set list

set scrolloff=5

" Use <Esc> to leave terminal mode
tnoremap <Esc> <C-\><C-n>
" Use "verbatim" mode to send the Esc sequence in terminal mode
tnoremap <C-v><Esc> <Esc>
map <F1> <NOP>
map <F2> <NOP>
map <F3> <NOP>
map <F4> <NOP>
map <F5> <NOP>
map <F6> <NOP>
map <F7> <NOP>
map <F8> <NOP>
map <F9> <NOP>
map <F10> <NOP>
map <F11> <NOP>
map <F12> <NOP>

imap <F1> <NOP>
imap <F2> <NOP>
imap <F3> <NOP>
imap <F4> <NOP>
imap <F5> <NOP>
imap <F6> <NOP>
imap <F7> <NOP>
imap <F8> <NOP>
imap <F9> <NOP>
imap <F10> <NOP>
imap <F11> <NOP>
imap <F12> <NOP>

"mapping pour redimensionner les fenêtres
map <Left> :vertical resize -5<CR>
map <Up> :resize +5<CR>
map <Right> :vertical resize +5<CR>
map <Down> :resize -5<CR>


nmap <Leader>ww :w<CR>

" -- Custom git mappings
nmap <Leader>la :Git -p la<CR>
nmap <Leader>d :Gvdiffsplit<CR>
nmap <Leader>D :Gvdiffsplit HEAD<CR>
nmap <Leader>gg :Git<CR>
nmap <Leader>ggs :Git show --name-status<CR>
nmap <Leader>gsb :Git branch<CR>
nmap <Leader>gsab :Git branch -ar<CR>
nmap <Leader>gsh :Git show HEAD<CR>

"on cursor, creates a line below and upside
nmap <Leader>O i<CR><ESC>O<ESC>o<ESC>O

"on cursor, add a CR and creates a line,
"used when creating objects / arrays
nmap <Leader>o i<CR><ESC>O

"Load plugins
nnoremap <Leader>l :nohl<CR>

nnoremap qq q:

" Open Tree explorer with focue on current file
nnoremap - :NvimTreeFindFile<CR>
nnoremap _ :NvimTreeToggle<CR>

nnoremap <C-w>t :term<CR>

" FIXME: port to lua
function DeleteHiddenBuffers()
    let tpbl=[]
    call map(range(1, tabpagenr('$')), 'extend(tpbl, tabpagebuflist(v:val))')
    for buf in filter(range(1, bufnr('$')), 'bufexists(v:val) && index(tpbl, v:val)==-1')
        silent execute 'bwipeout' buf
    endfor
endfunction

command! -bang CleanBuffers call DeleteHiddenBuffers()<CR>
nnoremap <Leader>cb :CleanBuffers<CR>

" FIXME: port to lua
" copy current filepath (value of % register) to + and unnamed registers for clipboard use
function! CopyFilepath ()
    let @+ = @%
    let @" = @%
endfunction
nnoremap <Leader>cp :call CopyFilepath()<CR>

function! RestartNs()
exe "ConjureEval (when-let [rs! (requiring-resolve (symbol (str *ns*)  \"restart!\"))] (rs!))"
endfunction

nmap <silent> <Leader>rs :execute RestartNs()<CR>

" nmap <silent> <localleader>cs :w \| :let filename= (nextjournal.clerk/show! \"  expand(%:p)  \")  \| :ConjureEval (nextjournal.clerk/show! \"  filename  \")<CR>
"
" Doing it like this because using a function makes the cursor jumps around
" the \| is a continuation symbol to inline a script
" Clerk commands
nmap <silent> <localleader>cs :w \| :let filename = expand("%:p") \| :let code = "(nextjournal.clerk/show! \"" .  filename . "\")"  \| :exe "ConjureEval " . code <CR>

" Clay commands
 " start a server using the current file path
nmap <silent> <localleader>cS :w \| :let filename = expand("%:p") \| :let code = "(notebook.clay/start! \"" .  filename . "\")"  \| :exe "ConjureEval " . code <CR>
 " remake the current file
 " You need to have a clay-config var in the ns
nmap <silent> <localleader>cm :w \| :let filename = expand("%:p") \| :let code = "(notebook.clay/make-doc! \"" .  filename . "\" (str *ns*))"  \| :exe "ConjureEval " . code <CR>


" Yank
noremap <silent> Y y$

"TABS Mappings
"put current buffer into its own tab
noremap <silent> <Leader>t :tab split<CR>
"put current buffer into its own tab removing it from the current window, noop
"if the buffer is the only presetn in the current window
nnoremap <Leader>T :wincmd T<CR>
"close current tab (does not close buffers)
noremap <silent> <Leader>tq :tabclose<CR>
noremap <silent> <Leader>to :tabonly<CR>
"creates a new tab
noremap <silent> <Leader>n :tabnew<CR>:Ex<CR>
noremap <silent> <Leader>L :tabnext<CR>
noremap <silent> <Leader>H :tabprevious<CR>
"navigate quickfix list
noremap <silent> <Leader>c :copen<CR>
noremap <silent> <Leader>K :cprev<CR>
noremap <silent> <Leader>J :cnext<CR>

set guifont=FiraCode\ Nerd\ Font\ Mono:h10


" LLM bindings

" write a custom vim mapping used in visual mode.

" Function to retrieve the text currently selected in visual mode
" kindly provided by https://stackoverflow.com/a/6271254/4374738
function! Get_visual_selection()
  " Get start and end positions of the selection
  let [line_start, column_start] = getpos("'<")[1:2]
  let [line_end, column_end] = getpos("'>")[1:2]

  " Retrieve the lines that are part of the visual selection
  let lines = getline(line_start, line_end)
  if len(lines) == 0
    return []
  endif

  " Trim the selection to the exact columns
  let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
  let lines[0] = lines[0][column_start - 1:]

  " Join lines with newline characters
  return lines
endfunction

" Function to run llm on the visual selection
function! Run_llm_on_visual_selection(...)
  let l:prompt = join(a:000)
  let l:visual_selection = Get_visual_selection()
  let l:fname = tempname()
  echo l:fname
  call writefile([l:prompt, ""], l:fname)
  call writefile(l:visual_selection, l:fname, "a")
  let l:code = 'term cat ' . l:fname . ' | llm -m 4o-mini -c | tee -a llm/answer.md'
  exe code
endfunction

command! -nargs=* -range Prompt call Run_llm_on_visual_selection(<f-args>)
